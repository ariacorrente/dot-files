# My configuration files

For easy installation on any terminal.

## Usage

Copy/symlink/append in the home folder the stuff required.

For example vim configuration can be installed running, from inside the root of
this cloned repository, the command:

    ln -s `pwd`/.vim $HOME/.vim    
