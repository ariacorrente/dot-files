srcFolder := $(realpath $(dir $(lastword $(MAKEFILE_LIST))))
dstFolder := $(HOME)

help:
	@echo "Paths used for installing the configuration files:"
	@echo "Source folder: " $(srcFolder)
	@echo "Destination folder: " $(dstFolder)

# If this git repository is in the same partition of the home directory simply
# link the configuration to allow automatic updates.
install-link: link-tmux-configuration link-vim-configuration

# If this git repository is in another partiotion a symbolic link is not
# possible. The solution is to copy the configurations at every change.
install-copy: copy-tmux-configuration copy-vim-configuration

.PHONY: link-tmux-configuration
link-tmux-configuration:
	ln -s $(srcFolder)/.tmux.conf $(dstFolder)/.tmux.conf

.PHONY: link-vim-configuration
link-vim-configuration:
	ln -s $(srcFolder)/.vim $(dstFolder)/.vim

.PHONY: copy-tmux-configuration
copy-tmux-configuration:
	cp -f $(srcFolder)/.tmux.conf $(dstFolder)/.tmux.conf

.PHONY: copy-vim-configuration
copy-vim-configuration:
	cp -rf $(srcFolder)/.vim $(dstFolder)/.vim

.PHONY: install-pathogen
install-pathogen:
	mkdir -p $(dstFolder)/.vim/autoload
	mkdir -p $(dstFolder)/.vim/bundle
	curl -LSso $(dstFolder)/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

.PHONY: install-fugitive
install-fugitive:
	cd $(dstFolder)/.vim/bundle && \
	git clone https://github.com/tpope/vim-fugitive.git && \
	vim -u NONE -c "helptags vim-fugitive/doc" -c q
